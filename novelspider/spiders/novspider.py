#-*-coding:utf8-*-
from scrapy.http import Request
from scrapy.contrib.spiders import CrawlSpider
from scrapy.selector import Selector
from novelspider.items import NovelspiderItem
import datetime
import copy
#Pycharm在这里会提示找不到novelspider,这是因为这个爬虫是放在program这个大的文件夹下的
#这属于误报，程序可以正常运行。

class novSpider(CrawlSpider):
    t=datetime.date(2015,01,01)
    now_date=datetime.datetime.now()
    now_date=datetime.date(now_date.year,now_date.month,now_date.day)

    name = "novspider"
    redis_key = 'nvospider:start_urls'
    start_urls = ['http://paper.people.com.cn']
    day_start_url = 'http://paper.people.com.cn/rmrb/html/'
    day_end_url='/nbs.D110000renmrb_'
    # 01.htm
    # day_base_url='http://paper.people.com.cn/rmrb/html/2015-01/01/'



    # def parse(self,response):
    #     yield Request(self.start_urls[0], callback=lambda response, typeid=5: self.parse_type(response,typeid))
    #
    # def parse_type(self,response, typeid):
    #     print typeid



    def parse(self, response):
        #因为第一次进循环就会被加一天
        t=self.t-datetime.timedelta(days=1)
        while t<self.now_date:
            t=t+datetime.timedelta(days=1)
            day_date=t.strftime('%Y-%m/%d')
            # print(day_date)

            day_url=self.day_start_url+day_date+self.day_end_url+'01.htm'
            #用一个嵌套函数来传递参数
            yield Request(day_url,callback=lambda response,t=t : self.parse_day(response,t))


    def parse_day(self,response,data_date):

        day_date=data_date.strftime('%Y-%m/%d')
        selector=Selector(response)
        list_typeArea=selector.xpath("//*[@id=\"pageList\"]/ul/*")
        #目前只能读前4页
        for i in xrange(1,4+1):
        # for i in xrange(1,len(list_typeArea)+1):
        #     print(day_date)
            num=str(i) if i>9 else '0'+str(i)
            typeArea_url=self.day_start_url+day_date+self.day_end_url+num+'.htm'
            yield Request(typeArea_url,callback=lambda response, t=data_date :self.parse_typeArea(response,t))
        self.t=self.t+datetime.timedelta(days=1)


    def parse_typeArea(self,response,data_date):
        day_date=data_date.strftime('%Y-%m/%d/')
        selector = Selector(response)
        list_l = selector.xpath("//*[@id=\"titleList\"]/ul/li/a/@href")

        list_url=[]
        for each in list_l:
            new_url=self.day_start_url+day_date+each.extract()
            list_url.append(new_url)
            yield Request(new_url,callback=lambda response,data_date=data_date: self.parse_content(response,data_date))

    def parse_content(self,response,data_date):

        selector=Selector(response)
        title=selector.xpath("/html/body/div[1]/div/div[2]/div[4]/div/h1/text()")
        title=title.extract()
        content_path=selector.xpath("//*[@id=\"ozoom\"]/p")
        content=''
        for i in content_path:
            # i=content_path[i]
            content+=i.extract()
        # print(content)
        item=NovelspiderItem()
        item['date']=str(data_date)
        item['title']=title
        item['content']=content
        yield item

